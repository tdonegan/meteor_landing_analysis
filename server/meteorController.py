import cherrypy
import re, json
from meteor_library import _meteor_database

class MeteorController(object):

	def __init__(self, mdb=None):
		if mdb is None:
			self.mdb = _meteor_database()
		else:
			self.mdb = mdb

		self.mdb.load_meteors('meteors.dat')


	def GET_KEY(self, meteor_id):
		# get meteor location and year based on id
		output = {'result' : 'success'}

		try:
			meteor_id = int(meteor_id)
			meteor = self.mdb.get_meteor(meteor_id)
			if meteor is not None:
				output['id'] = meteor_id
				output['location'] = meteor[0]
				output['year'] = int(meteor[1])
				output['mass'] = int(meteor[2])
				output['recclass'] = meteor[3]
				output['geolocation'] = meteor[4]

			else:
				output ['result'] = 'error'
				output['message'] = 'meteor not found'

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def PUT_KEY(self, meteor_id):
		# modify meteor location and year based on id
		output = {'result' : 'success'}

		try:
			meteor_id = int(meteor_id)
			data = json.loads(cherrypy.request.body.read().decode('utf-8'))
			meteor = [data['location'], int(data['year']), int(data['mass']), data['recclass'], data['geolocation']]
			self.mdb.set_meteor(meteor_id, meteor)
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def DELETE_KEY(self, meteor_id):
		# delete a meteor based on id
		output = {'result' : 'success'}

		try:
			meteor_id = int(meteor_id)
			self.mdb.delete_meteor(meteor_id)
		except Exception as ex:
			output['result'] = 'failure'
			output['message'] = str(ex)

		return json.dumps(output)

	def GET_INDEX(self):
		# get all meteors with their id, location, and year
		output = {'result' : 'success'}

		try:
			output['meteors'] = []

			for mid, location in self.mdb.meteors.items():
				year = self.mdb.years[mid]
				mass = self.mdb.mass[mid]
				recclass = self.mdb.recclass[mid]
				geolocation = self.mdb.geolocation[mid]
				dmeteor = {'id': mid, 'location' : location, 'year' : int(year), 'mass': int(mass), 'recclass': recclass, 'geolocation': geolocation}
				output['meteors'].append(dmeteor)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def POST_INDEX(self):
		# create and add a meteor's location and year
		output = { 'result' : 'success'}

		try:
			data = json.loads(cherrypy.request.body.read().decode('utf-8'))
			meteor = [data['location'], int(data['year']), data['mass'], data['recclass'], data['geolocation']]
			self.mdb.create_new_meteor(meteor)

		except Exception as ex:
			output['result'] = 'failure'
			output['message'] = str(ex)

		return json.dumps(output)

	def DELETE_INDEX(self):
		# delete all meteors
		output = {'result' : 'success'}

		try:
			self.mdb.delete_all_meteors()

		except Exception as ex:
			output['result'] ='failure'
			output['message'] = str(ex)

		return json.dumps(output)
