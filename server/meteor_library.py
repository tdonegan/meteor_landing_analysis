import requests
import json
from collections import defaultdict

#NOTE: This file is in the server directory temporarily while we attempt to get cross-folder importing working

class _meteor_database:

    def __init__(self):
        self.meteors = {}
        self.years = {}
        self.mass = {}
        self.recclass = {}
        self.geolocation = {}

    def load_meteors(self, meteor_file):
        # get json data and parse
        f = open(meteor_file)
        json_data = json.loads(f.read().replace("\n", ""))

        # go through json and store meteor data
        i = 0
        for meteor in json_data:
            try:
                #print(meteor)
                self.meteors[i] = meteor['name']
                self.years[i] = int(meteor['year'][:4])
                self.mass[i] = int(meteor['mass'])
                self.recclass[i] = meteor['recclass']
                self.geolocation[i] = [meteor['geolocation']['latitude'], meteor['geolocation']['longitude']]
                i += 1
            except: # just skip the meteor if there are exceptions
                pass

    def get_meteor_count(self):
        # returns a list of meteors and a list of years
        return len(self.meteors)

    def get_meteor(self, mid):
        # returns a list containing meteor name and year
        try:
            return [self.meteors[mid], self.years[mid], self.mass[mid], self.recclass[mid], self.geolocation[mid]]
        except:
            raise KeyError('Meteor ID not in database')

    def set_meteor(self, mid, meteor):
        # set a meteor's name and year based on mid
        if mid not in self.meteors:
            raise KeyError('Meteor ID not in database')
        try:
            self.meteors[mid] = meteor[0]
            self.years[mid] = int(meteor[1])
            self.mass[mid] = int(meteor[2])
            self.recclass[mid] = meteor[3]
            self.geolocation[mid] = meteor[4]
        except:
            raise ValueError('Invalid meteor entry')

    def create_new_meteor(self, meteor):
        # create a new meteor with name and year
        if self.meteors:
            mid = max(self.meteors.keys()) + 1
        else:
            mid = 0

        try:
            self.meteors[mid] = meteor[0]
            self.years[mid] = int(meteor[1])
            self.mass[mid] = int(meteor[2])
            self.recclass[mid] = meteor[3]
            self.geolocation[mid] = meteor[4]
        except:
            raise ValueError('Invalid meteor entry')

    def get_year(self, user_year):
        # get all meteors from a specific year
        mets = []
        for mid, year in self.years.items():
            if int(year) == int(user_year):
                mets.append(self.meteors[mid])
        return mets

    def get_all_years(self):
        # sort meteors into years
        year_buckets = defaultdict(list)
        for i in range(len(self.meteors)):
            if i in self.years:
                year = int(self.years[i])
                meteor = self.meteors[i]
                year_buckets[year].append(meteor)

        return year_buckets

    def set_year(self, mid, year):
        # set year for a meteor based on mid
        if mid in self.years:
            self.years[mid] = int(year)
        else:
            raise KeyError('Meteor ID not in database')

    def delete_meteor(self, mid):
        # delete a meteor based on mid
        try:
            del(self.meteors[mid])
            del(self.years[mid])
            del(self.mass[mid])
            del(self.recclass[mid])
            del(self.geolocation[mid])

        except:
            raise KeyError('Meteor ID not in database')

    def delete_all_meteors(self):
        # delete all meteors
        self.meteors = {}
        self.years = {}

if __name__ == '__main__':
       mdb = _meteor_database()
       mdb.load_meteors('meteors.dat')

       for i in range(len(mdb.meteors)):
           print(mdb.meteors[i], mdb.years[i], mdb.mass[i], mdb.recclass[i], mdb.geolocation[i])
