import cherrypy
import routes
from meteorController import MeteorController
from resetController import ResetController
from yearController import YearController
from meteor_library import _meteor_database

def start_service():
	dispatcher = cherrypy.dispatch.RoutesDispatcher()

	# load initial meteor data
	mdb = _meteor_database()
	meteorController = MeteorController(mdb=mdb)
	resetController = ResetController(mdb=mdb)
	yearController = YearController(mdb=mdb)

	# meteor controller endpoints
	dispatcher.connect('meteor_get', '/meteors/:meteor_id', controller=meteorController, action = 'GET_KEY', conditions=dict(method=['GET']))
	dispatcher.connect('meteor_put', '/meteors/:meteor_id', controller=meteorController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('meteor_delete', '/meteors/:meteor_id', controller=meteorController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
	dispatcher.connect('meteor_index_get', '/meteors/', controller=meteorController, action = 'GET_INDEX', conditions=dict(method=['GET']))
	dispatcher.connect('meteor_index_post', '/meteors/', controller=meteorController, action = 'POST_INDEX', conditions=dict(method=['POST']))
	dispatcher.connect('meteor_index_delete', '/meteors/', controller=meteorController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))

    # reset controller endpoints
	dispatcher.connect('reset_put', '/reset/:meteor_id', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    # year controller endpoints
	dispatcher.connect('year_get', '/years/:year', controller=yearController, action='GET_KEY', conditions=dict(method=['GET']))
	dispatcher.connect('year_index_get', '/years/', controller=yearController, action='GET_INDEX', conditions=dict(method=['GET']))

	# default OPTIONS handler for CORS, all direct to the same place
	dispatcher.connect('meteor_options', '/meteors/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('meteor_key_options', '/meteors/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('reset_options', '/reset/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('reset_key_options', '/reset/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('year_options', '/years/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('year_key_options', '/years/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

	conf = {
		'global' : {
			#'server.socket_host' : 'student04.cse.nd.edu',
			'server.socket_host' : 'localhost',
			'server.socket_port': 51058
		},
	    '/': {
		    'request.dispatch' : dispatcher,
            'tools.CORS.on' : True,
		}
	}

	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

# class for CORS
class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

# function for CORS
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
	start_service()
