import unittest
import requests
import json

class TestServer(unittest.TestCase):

	#SITE_URL = 'http://student04.cse.nd.edu:51058' # temp URL
	SITE_URL = 'http://localhost:51058'
	METEOR_URL = SITE_URL + '/meteors/'
	YEAR_URL = SITE_URL + '/years/'
	RESET_URL = SITE_URL + '/reset/'

	def is_json(self, resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def reset_data(self):
		m = {}
		r = requests.delete(self.METEOR_URL, data=json.dumps(m))

	def reset_to_meteor_data(self):
		r = requests.put(self.RESET_URL)

	def test_meteors_get_key(self):
		self.reset_to_meteor_data()

		# check if data is retrieved successfully
		key = '9999999999'
		r = requests.get(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'error')

		key = '0'
		r = requests.get(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['location'], 'Aachen')

	def test_meteors_get_index(self):
		self.reset_to_meteor_data()

		# check for successful reset
		r = requests.get(self.METEOR_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# check for test index
		meteors = resp['meteors']
		test = meteors[0]
		self.assertEqual(test['location'], 'Aachen')

	def test_meteors_put_key(self):
		self.reset_data()

		# input test data
		key = '0'
		m = {}
		m['location'] = 'Notre Dame'
		m['year'] = 2021
		m['mass'] = 3000
		m['recclass'] = 'L6'
		m['geolocation'] = ['41.6177', '86.2490']
		r = requests.post(self.METEOR_URL, data=json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		m['location'] = 'South Bend'
		m['year'] = 2022
		m['mass'] = 3000
		m['recclass'] = 'L6'
		m['geolocation'] = ['41.6177', '86.2490']
		r = requests.put(self.METEOR_URL + key, data=json.dumps(m))

		# check for successful input
		r = requests.get(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['location'], m['location'])

	def test_meteors_delete_key(self):
		self.reset_data()

		# input sample for deletion
		key = '0'
		m = {}
		m['location'] = 'Notre Dame'
		m['year'] = 2021
		m['mass'] = 3000
		m['recclass'] = 'L6'
		m['geolocation'] = ['41.6177', '86.2490']
		r = requests.post(self.METEOR_URL, data=json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# delete sample
		r = requests.delete(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# check for successful deletion
		r = requests.get(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'error')

	def test_meteors_delete_index(self):
		self.reset_to_meteor_data()

		# delete the samples
		r = requests.delete(self.METEOR_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# check for successful deletion
		r = requests.get(self.METEOR_URL + '0')
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'error')

		r = requests.get(self.METEOR_URL + '1')
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'error')

	def test_meteors_post_index(self):
		self.reset_data()

		# post sample data
		m = {}
		m['id'] = 0
		m['location'] = 'Notre Dame'
		m['year'] = 2021
		m['mass'] = 3000
		m['recclass'] = 'L6'
		m['geolocation'] = ['41.6177', '86.2490']
		r = requests.post(self.METEOR_URL, data=json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success') #

		# get all data
		r = requests.get(self.METEOR_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# check for test index
		meteors = resp['meteors']
		test = meteors[0]
		self.assertEqual(test['id'], m['id'])
		self.assertEqual(test['location'], m['location'])

	def test_years_get_key(self):
		self.reset_to_meteor_data()

		# check if data exists
		key = '1900'
		r = requests.get(self.YEAR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		locations = ["Alexandrovsky", "Emmaville", "Felix", "Forsbach", "Leonovka", "N'Goureyma", "Ofeherto"]
		self.assertEqual(resp['locations'], locations)


	def test_years_get_index(self):
		self.reset_to_meteor_data()

		# check if sample data exists
		r = requests.get(self.YEAR_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# Still unsure about indexing
		locations = ["Alexandrovsky", "Emmaville", "Felix", "Forsbach", "Leonovka", "N'Goureyma", "Ofeherto"]
		self.assertEqual(resp['1900'], locations)
		locations = ["Heredia", "Kaba", "Les Ormes", "Ohaba", "Parnallee", "Quenggouk", "Stavropol"]
		self.assertEqual(resp['1857'], locations)

	def test_reset_put_key(self):
		self.reset_data()
		key = '0'

		# input sample data
		m = {}
		m['location'] = 'Notre Dame'
		m['year'] = 2021
		m['mass'] = 3000
		m['recclass'] = 'L6'
		m['geolocation'] = ['41.6177', '86.2490']
		r = requests.post(self.METEOR_URL, data=json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success') #

		# check data was received and altered
		r = requests.get(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		self.assertEqual(resp['location'], 'Notre Dame')

		# reset at index
		m = {}
		r = requests.put(self.RESET_URL + key, data=json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# check for successful reset
		r = requests.get(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		self.assertEqual(resp['location'], 'Aachen')

	def test_reset_put_index(self):
		self.reset_data()

		# input sample data
		key = '0'
		m = {}
		m['location'] = 'Notre Dame'
		m['year'] = 2021
		m['mass'] = 3000
		m['recclass'] = 'L6'
		m['geolocation'] = ['41.6177', '86.2490']
		r = requests.post(self.METEOR_URL, data=json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success') #

		# initiate reset
		m = {}
		r = requests.put(self.RESET_URL, data=json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# data should be something else now
		r = requests.get(self.METEOR_URL + key)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertNotEqual(resp['location'], 'Notre Dame')

if __name__ == '__main__':
	unittest.main()
