const urlParams = new URLSearchParams(window.location.search);
var id = urlParams.get('id');
let map, infoWindow;
makeNetworkCallToMeteorApi(id);

function makeNetworkCallToMeteorApi(id){
  console.log('entered makeNetworkCallToAgeApi');
  var url = "http://localhost:51058/meteors/" + id;
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);

  xhr.onload = function(e){
      //console.log('network response received' + xhr.responseText);
      parseMeteorResponse(xhr.responseText);
  }

  xhr.onerror = function(e){
      console.error(xhr.statusText);
  }

  xhr.send(null);
}

function parseMeteorResponse(responseText){
  var meteor = JSON.parse(responseText);
  var container = document.getElementById('info');

  var title = document.createElement('h1');
  title.innerHTML = meteor["location"] + ', ' + meteor["year"];
  container.appendChild(title);

  var list = document.createElement('ul');

  var mass = document.createElement('li');
  mass.innerText = "Mass: " + meteor['mass'] + " grams";
  list.appendChild(mass);

  var recclass = document.createElement('li');
  recclass.innerText = "Classification: " + meteor['recclass'];
  list.appendChild(recclass);

  var country = document.createElement('li');
  country.innerText = "Country: " + urlParams.get('country');
  list.appendChild(country);

  container.appendChild(list);
  initMap(meteor);
}

// Initialize and add the map
function initMap(meteor) {
  console.log('inside init map');
  // The location of Uluru
  var latitude = parseFloat(meteor['geolocation'][0]);
  var longitude = parseFloat(meteor['geolocation'][1]);
  console.log(latitude, longitude);
  const location = { lat: latitude, lng: longitude };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 6,
    center: location,
  });
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: location,
    map: map,
  });
}
