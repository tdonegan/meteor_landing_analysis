console.log('page load - entered all_meteors.js');
var table = document.getElementById('meteor-table');
var tableBody = document.createElement("tbody");
makeNetworkCallToMeteorApi();
table.appendChild(tableBody);

// TODO: onclick search functionality for search button
var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = runSearch;

function runSearch(){
  var searchText = document.getElementById('search-box').value.toLowerCase();
  tr = table.getElementsByTagName('tr');

  for(let i = 1; i < tr.length; i++){
    td = tr[i].getElementsByTagName('td');
    //console.log(td.length);
    var txtValue = td[0].getElementsByTagName('a')[0].innerHTML;
    if (txtValue.toLowerCase().indexOf(searchText) > -1) {
      tr[i].style.display = "";
      continue;
    } else {
      tr[i].style.display = "none";
    }
    txtValue = td[1].innerHTML;
    if (txtValue.toLowerCase().indexOf(searchText) > -1) {
      tr[i].style.display = "";
      continue;
    } else {
      tr[i].style.display = "none";
    }
    txtValue = td[2].innerHTML;
    if (txtValue.toLowerCase().indexOf(searchText) > -1) {
      tr[i].style.display = "";
      continue;
    } else {
      tr[i].style.display = "none";
    }
  }
}

function makeNetworkCallToMeteorApi(){
    console.log('entered makeNetworkCallToAgeApi');
    var url = "http://localhost:51058/meteors/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e){
        //console.log('network response received' + xhr.responseText);
        parseMeteorResponse(xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function parseMeteorResponse(response_text){
  var response_json = JSON.parse(response_text);
  if(response_json['result'] == "success"){
    var meteors = response_json['meteors'];

    meteors.forEach(meteor => {
      makeNetworkCallToTeleportApi(meteor);
    });
  }
}

function makeNetworkCallToTeleportApi(meteor){
  var url = "https://api.teleport.org/api/cities/?search=" + meteor['location'];
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);

  xhr.onload = function(e) {
      var response_json = JSON.parse(xhr.responseText);

      try{
        var country = response_json['_embedded']['city:search-results'][0]['matching_full_name'];
        displayMeteor(meteor, country.split(',')[2].split('(')[0]);
      }
      catch(err){
        return null;
      }
  }

  xhr.onerror = function(e){
      console.error(xhr.statusText);
      return null;
  }

  xhr.send(null);
  return null;
}

function displayMeteor(meteor, country){
  var row = document.createElement("tr");

  var cell = document.createElement("td");
  cell.innerHTML = '<a href="individual_page.html?id=' + meteor['id'] + '&country=' + country + '">' + meteor['location'] + '</a>';
  row.appendChild(cell);

  cell = document.createElement("td");
  cell.innerHTML = country;
  row.appendChild(cell);

  cell = document.createElement("td");
  cell.innerHTML = meteor['year'];
  row.appendChild(cell);

  tableBody.appendChild(row);
}
