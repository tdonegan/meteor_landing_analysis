console.log('page load - entered all_years.js');
var table = document.getElementById('years-table');
makeNetworkCallToMeteorApi();

function makeNetworkCallToMeteorApi(){
    console.log('entered makeNetworkCallToAgeApi');
    var url = "http://localhost:51058/years/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e){
        //console.log('network response received' + xhr.responseText);
        parseYearResponse(xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function parseYearResponse(response_text){
  var years = JSON.parse(response_text);
  var count = 0;
  var row = document.createElement("tr");
  if(years['result'] == "success"){
    Object.keys(years).forEach(function(key) {
      if(count % 3 == 0){
        row = document.createElement("tr");
      }
      if(key != 'result'){
        row = displayYear(row, key, years[key]);
      }
      if(count % 3 == 2){
        table.appendChild(row);
      }
      count += 1;
    })
  }
}

function displayYear(row, year, locations){
  var yearEntry = document.createElement("td");
  //yearEntry.classList.add("col-sm-4");

  var title = document.createElement("h4");
  title.innerHTML = year;
  yearEntry.appendChild(title);

  var yearList = document.createElement("ul");

  locations.forEach(location => {
    var place = document.createElement("li");
    place.innerHTML = location;
    yearList.appendChild(place);
  });

  yearEntry.appendChild(yearList);
  row.appendChild(yearEntry);
  return row;
}

function displayMeteor(meteor, country){
  var row = document.createElement("tr");

  var cell = document.createElement("td");
  cell.innerHTML = '<a href="individual_page.html?id=' + meteor['id'] + '&country=' + country + '">' + meteor['location'] + '</a>';
  row.appendChild(cell);

  cell = document.createElement("td");
  cell.innerHTML = country;
  row.appendChild(cell);

  cell = document.createElement("td");
  cell.innerHTML = meteor['year'];
  row.appendChild(cell);

  table.appendChild(row);
}
