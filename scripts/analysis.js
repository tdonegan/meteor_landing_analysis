var ctx = document.getElementById('infoChart');
makeNetworkCallToMeteorApi();

function makeNetworkCallToMeteorApi(){
    console.log('entered makeNetworkCallToMeteorApi');
    var url = "http://localhost:51058/years/";
    // var url = "http://student04.cse.nd.edu:51058/years/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e){
        //console.log('network response received' + xhr.responseText);
        parseYearResponse(xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function parseYearResponse(response_text){
  var years = JSON.parse(response_text);
  var data = [0,0,0,0,0,0];
  if(years['result'] == "success"){
    //Object.keys(years).forEach(function(key) {
    for([key, values] of Object.entries(years)) {
      console.log(key);
//      console.log(values);
      console.log(values.length);
        if(parseInt(key) <= 1800) {
            data[0] += parseInt(values.length);
        } else if(parseInt(key) <= 1850) {
            data[1] += parseInt(values.length);
        } else if(parseInt(key) <= 1900) {
            data[2] += parseInt(values.length);
        } else if(parseInt(key) <= 1950) {
            data[3] += parseInt(values.length);
        } else if(parseInt(key) <= 2000) {
            data[4] += parseInt(values.length);
        } else {
            data[5] += parseInt(values.length);
        }
    };
    console.log(data);
  }
  //var data = [1,2,3,4,5,6];
  createChart(data);
}

function createChart(data_set) {
    var infoChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['0-1800', '1801-1850', '1851-1900', '1901-1950', '1951-2000', '2001-present'],
            datasets: [{
                label: 'Meteors by Centuries',
                data: [data_set[0], data_set[1], data_set[2], data_set[3], data_set[4], data_set[5]],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}
