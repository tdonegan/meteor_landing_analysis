# Meteor_Landing_Analysis

### Group Members:

    - Ted Donegan  (tdonegan@nd.edu)
    - Greg Gavenda (ggavenda@nd.edu)

### Scale and Complexity:

    - Contains about 1000 meteor data points, parsed with JSON
    - Use OO cherrypy server, which the web page accesses
    - Make requests to teleport API to match locations to countries
    - Search capabilities within the meteors page
    - Set up an API key to make requests to and embed Google Maps based on geolocation
    - Use Chart.js to generate graphs

### OO API:

    Start the server from within the server directory:
        $ python3 server.py
    
    Run tests from within the ooapi directory: 
        $ python3 test_api.py

### JSON Specification

| Command   | Resource      | Input             | Output                    |
|-----------|-----------|-----------|-----------|
| GET       | /meteors/ | {}        | {‘result’ : ‘success’, ‘meteors’ : [{‘id’: 1, ‘location’: ‘Aachen’, ‘year’ : 1880, 'mass': 2000, 'recclass': 'L5', 'geolocation': ['50.775', '6.08333']},{‘id’: 2, ‘location’: Aarhus‘’, ‘year’ : 1951, ...}, ... ] }|
| GET       | /meteors/:id | {}        | {‘results’ : ‘success’, ‘id’ : 1, ‘location’ : ‘Aachen’, ‘year’ : 1880, 'mass': 2000, 'recclass': 'L5', 'geolocation': ['50.775', '6.08333']} |
| PUT       | /meteors/:id | {‘location’ : ‘Aachen’, ‘year’ : 1881, 'mass': 2000, 'recclass': 'L5', 'geolocation': ['50.775', '6.08333']} | {‘result’ : ‘success’} |
| DELETE    | /meteors/ | {}        | {‘result’ : ‘success’} |
| DELETE    | /meteors/:id | {}        | {‘result’ : ‘success’} |
| POST      | /meteors  | {‘location’ : ‘Notre Dame’, ‘year’, 2021, 'mass': 2100, 'recclass': 'L5', 'geolocation': ['50.775', '6.08333']} | {‘result’ : ‘success’} |
| GET       | /years/   | {}        | {‘result’ : ‘success’, ‘861’ : [‘Nogata’, ...], ‘921’ : [‘Narni’, ...], ...} |
| GET       | /years/:year | {}        | {‘result’: ‘success’, ‘year': 1900, 'locations': ['Alexandrovsky', 'Emmaville', 'Felix', ...]} |
| PUT       | /reset/   | {}        | {‘result’ : ‘success’} |
| PUT       | /reset/:id | {}        | {‘result’ : ‘success’} |

